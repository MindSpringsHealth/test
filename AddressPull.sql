USE AdventureWorks

SELECT *
FROM Person.Address a
JOIN HumanResources.EmployeeAddress ea
ON ea.AddressID = a.AddressID
JOIN HumanResources.Employee e
ON e.EmployeeID = ea.EmployeeID