USE AdventureWorks

SELECT e.LoginID
		,e.Title
		,e.Gender
		,edh.StartDate
		,edh.EndDate
		,d.Name
		,d.GroupName
FROM HumanResources.Employee e
JOIN HumanResources.EmployeeDepartmentHistory edh
ON edh.EmployeeID = e.EmployeeID
JOIN HumanResources.Department d
ON d.DepartmentID = edh.DepartmentID
WHERE edh.EndDate IS NULL